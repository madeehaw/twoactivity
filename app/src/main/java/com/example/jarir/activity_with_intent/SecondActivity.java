package com.example.jarir.activity_with_intent;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        EditText output1 = (EditText) findViewById(R.id.output1);
        EditText output2 = (EditText) findViewById(R.id.output2);

        Intent intent = getIntent();
        output1.setText(intent.getStringExtra("userName"));
        output2.setText(intent.getStringExtra("passWord"));

        Button backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SecondActivity.this,"Back to 1st Activite",Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(SecondActivity.this,MainActivity.class);
                                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "Second:On stop", Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Second:On Destroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Second:On Pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Second:On resume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Second:On Start", Toast.LENGTH_SHORT).show();
    }
}
